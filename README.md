# Test g2o

too simplified C++ code to use g2o with cmake

## FindG2O.cmake
This file is a modified version of its original available on the original g2o repository.
The modification is as follows:  
- Add header information
  - explanation of a cmake variable G2O\_FOUND
  - explanation of a cmake variable G2O\_INCLUDE\_DIR
  - explanation of a cmake variable G2O\_LIBRARIES
- Add cmake variables
  - define a cmake variable G2O\_FOUND that is YES if g2o is found and NO otherwise
  - define a cmake variable G2O\_INCLUDE\_DIR that contains all include directories
  - define a cmake variable G2O\_LIBRARIES that contains all libraries

## EXTERNAL/ceres
This directory is copied from original repository to enable ceres' auto differentiation functionaly.  
g2o requires the header files stored in the directory but g2o's make install does not copy those files.
Therefore, I copied the directory.
